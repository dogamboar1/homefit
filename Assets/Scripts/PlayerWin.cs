﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Mirror.Scenes;
using TMPro;
using UnityEngine;


[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(PlayerNetworkController))]
public class PlayerWin : MonoBehaviour
{
    public bool llego = false;
    private GameManager gm;
    private TextMeshProUGUI resultTxt;
    private PlayerNetworkController _playerNetworkController;
    
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        resultTxt = gm.winCanvas.GetComponentInChildren<ResultText>(true).GetComponent<TextMeshProUGUI>();
        _playerNetworkController = gameObject.GetComponent<PlayerNetworkController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("OnTriggerEnter");
        if (other.gameObject.CompareTag("Meta"))
        {
            
            Debug.Log("Contra la meta & local player");
            string result = "";
            llego = true;
            if (gm.winnerPlayer == null)
            {
                result = "Ganaste";
                //GetComponent<Animator>().SetBool("Moverse", false);
                GetComponent<Animator>().SetBool("Gano", true);
                gm.winnerPlayer = gameObject;
            }
            else 
            {
                GetComponent<Animator>().SetBool("Perder", true);
                result = "Perdiste";
                gm.loserPlayer = gameObject;
            }
            
            if (_playerNetworkController.isLocalPlayer)
            {
                gm.subState = GameManager.gameSubStates.Finish;
                resultTxt.text = result;
            }
        }
    }
}

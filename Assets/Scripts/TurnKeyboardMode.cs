﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnKeyboardMode : MonoBehaviour
{
    
    private KeyboardManager _keyboardManager;
    public bool externalKeyboard;
    public Toggle checkbox;
    
    private void Start()
    {
        _keyboardManager = GameObject.FindWithTag("KeyboardController").GetComponent<KeyboardManager>();
        checkbox = gameObject.GetComponent<Toggle>();
        externalKeyboard = _keyboardManager.activate;
        _keyboardManager.activate = externalKeyboard;
        checkbox.isOn = externalKeyboard;
    }

    public void SetKeyboardMode()
    {
        externalKeyboard = checkbox.isOn;
        _keyboardManager.activate = externalKeyboard;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using TMPro;

public class SetKeyboardPort : MonoBehaviour
{
    private TMP_Dropdown selector;
    public List<string> OptionList = new List<string>();
    private KeyboardManager _keyboardManager;

    // Start is called before the first frame update
    void Start()
    {
        selector = gameObject.GetComponent<TMP_Dropdown>();
        ReadAvailablePorts();
    }

    // Update is called once per frame
    void Update()
    {
        _keyboardManager = GameObject.FindWithTag("KeyboardController").GetComponent<KeyboardManager>();
    }

    void ReadAvailablePorts()
    {
        // Get a list of serial port names.
        string[] ports = SerialPort.GetPortNames();

        Debug.Log("The following serial ports were found:");
        OptionList.Add("");

        // Display each port name to the console.
        foreach(string port in ports)
        {
            //Debug.Log(port);
            OptionList.Add(port);
        }
        
        selector.AddOptions(OptionList);
    }

    public void SetKeyboardPortOnChange()
    {
        _keyboardManager.port = OptionList[selector.value];
    } 
}

﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Serialization;
using Mirror.Scenes;
using UnityEngine.UI;

public class GameManager : NetworkBehaviour
{

    public enum gameStates {GameLoaded, Home, ConnectPlayer, SelectWorld, Playing, GameOver , Loading, Results, Settings};
    public gameStates state = gameStates.Home;
    
    public enum gameSubStates {Start, Prepare, Run, Finish };
    public gameSubStates subState = gameSubStates.Start;
    
    public GameObject NetworkManager;
    private OurNetworkManager _OurNetworkManager;
    
    public GameObject homeCanvas, connectMultiplayerCanvas, gameOverCanvas, selectWorldCanvas, loadAppCanvas, winCanvas, gameCanvas, configCanvas;

    public GameObject gameUI, gameWaiting;
    private float contador = 0f;
    private float contadorEnd = 0f;
    
    public GameObject meta;
    
    public JugadorManager _jugadorManager;

    private PlayerNetworkController _PlayerController;

    private Countdown _countdown;

    public GameObject winnerPlayer;
    public GameObject loserPlayer;

    public AudioClip botones;
    public AudioSource bocina;

    public GameObject backgroundSound, backgroundGameSound;

    // Start is called before the first frame update
    void Start()
    {
        _jugadorManager = GameObject.Find("JugadorManager").GetComponent<JugadorManager>();
        _countdown = gameUI.GetComponent<Countdown>();
        _OurNetworkManager = NetworkManager.GetComponent<OurNetworkManager>();

        state = gameStates.GameLoaded;
        backgroundSound.SetActive(false);
        backgroundGameSound.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        switch (state)
        {
            case gameStates.GameLoaded:
                loadAppCanvas.SetActive(true);
                contador += Time.deltaTime;
                if (contador >= 3f)
                {
                    hideCanvas();
                    state = gameStates.Home;
                    contador = 0f;
                }
                break;
            case gameStates.Home:
                backgroundSound.SetActive(true);
                homeCanvas.SetActive(true);
                break;
            case gameStates.Playing:
                gameCanvas.SetActive(true);
                checkSubState();
                break;
            case gameStates.GameOver:
                gameOverCanvas.SetActive(true);
                break;
            case gameStates.ConnectPlayer:
                connectMultiplayerCanvas.SetActive(true);
                break;
            case gameStates.SelectWorld:
                selectWorldCanvas.SetActive(true);
                break;
            case gameStates.Results:
                winCanvas.SetActive(true);
                break;
            case gameStates.Settings:
                configCanvas.SetActive(true);
                break;
        }
    }

    void checkSubState()
    {
        backgroundSound.SetActive(false);
        backgroundGameSound.SetActive(true);
        switch (subState)
        {
            case gameSubStates.Start:
                gameUI.SetActive(false);
                gameWaiting.SetActive(true);
                break;
            case gameSubStates.Prepare:
                gameWaiting.SetActive(false);
                gameUI.SetActive(true);
                if (_countdown.finished)
                {
                    subState = gameSubStates.Run;
                }
                break;
            case gameSubStates.Run:
                gameWaiting.SetActive(false);
                break;
            case gameSubStates.Finish:
                if (GameIsFinished())
                {
                    contadorEnd += Time.deltaTime;
                    if (contadorEnd >= 4f)
                    {
                        contadorEnd = 0f;
                        state = gameStates.Results;
                    }
                }
                break;
        }
        
        // Los dos jugadores estan listos para jugar
        if (_jugadorManager.IsGameReady && subState == gameSubStates.Start)
        {
            setLocalPlayer();
            subState = gameSubStates.Prepare;
            _countdown.active = true;
        }
        
        if (subState == gameSubStates.Run && _PlayerController)
        {
            _PlayerController.canRun = true;
        }
        else if (_PlayerController) _PlayerController.canRun = false;
        
    }

    bool GameIsFinished()
    {
        if (_jugadorManager.MinimumPlayersForGame > 1 && loserPlayer != null && winnerPlayer != null)
        {
            return true;
        } else if (_jugadorManager.MinimumPlayersForGame == 1 && winnerPlayer != null)
        {
            return true;
        }

        return false;
    }

    void setLocalPlayer()
    {
        if (!_PlayerController)
        {
            _PlayerController = ClientScene.localPlayer.GetComponent<PlayerNetworkController>();
        }
    }

    void hideCanvas()
    {
        homeCanvas.SetActive(false);
        connectMultiplayerCanvas.SetActive(false);
        selectWorldCanvas.SetActive(false);
        loadAppCanvas.SetActive(false);
        winCanvas.SetActive(false);
        configCanvas.SetActive(false);
        backgroundGameSound.SetActive(false);
    }

    public void GOToHome()
    {
        Debug.Log("GOToHome");
        RestartGame();
        hideCanvas();
        state = gameStates.Home;
        bocina.PlayOneShot(botones);
    }

    public void GOToConnectPlayer()
    {
        Debug.Log("GOToConnectPlayer");
        RestartGame();
        hideCanvas();
        state = gameStates.ConnectPlayer;
        bocina.PlayOneShot(botones);
    }

    public void GOToPreLoading()
    {
        Debug.Log("GOToLoad");
        hideCanvas();
        state = gameStates.Loading;
        bocina.PlayOneShot(botones);
    }

    public void GOToGameOver()
    {
        Debug.Log("GOToGameOver");
        hideCanvas();
        state = gameStates.GameOver;
        bocina.PlayOneShot(botones);
    }

    public void GOToWin()
    {
        Debug.Log("GOToWin");
        winCanvas.SetActive(true);
    }
    
    public void GOToSelect()
    {
        Debug.Log("GOToSelect");
        hideCanvas();
        state = gameStates.SelectWorld;
        bocina.PlayOneShot(botones);
    }
    
    public void GOToPlay()
    {
        Debug.Log("GOToPlay");
        hideCanvas();
        state = gameStates.Playing;
        subState = gameSubStates.Start;
        bocina.PlayOneShot(botones);
    }
    
    public void GOToConfiguration()
    {
        Debug.Log("GOToConfiguration");
        hideCanvas();
        state = gameStates.Settings;
        bocina.PlayOneShot(botones);
    }

    void RestartGame()
    {
        contadorEnd = 0f;
        _countdown.Restart();
        winnerPlayer = null;
        loserPlayer = null;
        if (_OurNetworkManager.isNetworkActive)
        {
            _OurNetworkManager.StopClient();
            _OurNetworkManager.StopServer();
        }
    }

}

